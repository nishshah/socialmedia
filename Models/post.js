const mongoose = require('mongoose')

const post = mongoose.Schema({
    userName:{type: mongoose.Schema.Types.ObjectId,ref:'User'},
    content:{type:String, require:true},
    locations:{type:String},
    tags:{type:String}
})

module.exports=mongoose.model('CreatePost',post)
