const mongoose = require('mongoose')

const status= mongoose.Schema({
    id:{type: mongoose.Schema.Types.ObjectId,ref:'CreatePost'},
    like:{type:Number,default:0},
    comment:{type:Array[String],default:" "},
    byUser:{type: mongoose.Schema.Types.ObjectId,ref:'User'}
},{ timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at'})



module.exports=mongoose.model('postStatus',status)