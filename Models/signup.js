const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const user = new mongoose.Schema({
    name:{type: String, required:true},
    mail:{type: String, required:true},
    password:{type:String, required:true},
    number:{type: Number},
    profilePic:{type: String}
},
{ time:{timestamps: true} })

user.pre('save',async function (next){
    try {
        const salt = await bcrypt.genSalt(10)
        const hashPassword =  await bcrypt.hash(this.password , salt)
        this.password= hashPassword
    } catch (error) {
        next(error)
    }
})

module.exports=mongoose.model('User',user)