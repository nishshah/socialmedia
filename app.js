const express = require('express')
const user = require('./Controlles/userRoute')
const mongose = require('mongoose')
const bodyparser = require('body-parser')

const app = express()
mongose.connect('mongodb://localhost/ExData',{
    useNewUrlParser: true })   
    .then(() => console.log("Database connected!"))
    .catch(err => console.log(err))
   
 app.use(bodyparser.urlencoded({extended: false}))
   app.use(bodyparser.json())
app.use('/user',user)

app.listen(3000,()=>{
    console.log('Server at localhost:3000')
})